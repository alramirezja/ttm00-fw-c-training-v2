/* 
 * File:   main.c
 * Author: user8
 *
 * Created on July 9, 2021, 5:32 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include"main.h"
#include "config_bits.h"

#define RX_BUFFER_LENGTH 64
#define TX_BUFFER_LENGTH 128

volatile uint32_t sys_tick = 0; // time base for the system in ms

uint8_t uart1_rx_buffer[RX_BUFFER_LENGTH];
uint8_t uart1_tx_buffer[TX_BUFFER_LENGTH];

uart_driver_t uart1;
uart_driver_config_t uart1_config = {
    .uart = UART1,
    .baud_rate = 115200,
    .rx_buffer = uart1_rx_buffer,
    .rx_max_length = RX_BUFFER_LENGTH,
    .tx_buffer = uart1_tx_buffer,
    .tx_max_length = TX_BUFFER_LENGTH
};

void core_timer_init(void);
void gpio_init(void);


/*
 * 
 */
void main(void) {
    /* Initialize peripherals */
    core_timer_init();
    gpio_init();
    uart_driver_init(&uart1, &uart1_config);
    
    /* Configure and enable the interrupts of the system */
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
    while (1) {

    }
}



void core_timer_init(void)
{
    /* Set the period of the timer to 1ms */
    OpenCoreTimer(CORE_TICK_RATE);
    
    /* Configure the interruption of the core timer */
    mConfigIntCoreTimer((CT_INT_ON | CT_INT_PRIOR_7 | CT_INT_SUB_PRIOR_0));
}

void gpio_init(void)
{
    mPORTBSetPinsDigitalOut(BIT_2 | BIT_3 | BIT_4);
    mPORTBSetBits(BIT_2 | BIT_3 | BIT_4);
    
    /* UART1 PPS
     * RPF1 -> UART1_RX
     * RPF0 -> UART1_TX
     */
    U1RXR = 0b0100;
    RPF0R = 0b0011;
}

