
#include"main.h"

extern uart_driver_t uart1;

void __ISR(_CORE_TIMER_VECTOR, IPL7_AUTO) core_timer_interrupt_handler(void)
{
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);

    sys_tick++;
}

void __ISR(_UART1_VECTOR, IPL2SOFT) uart1_interrupt_handler(void)
{

    if(INTGetFlag(INT_SOURCE_UART_RX(UART1))) {
        uart_driver_receive_rx_data(&uart1);
        INTClearFlag(INT_SOURCE_UART_RX(UART1));

    }        
    if (INTGetFlag(INT_SOURCE_UART_TX(UART1))) {
        // Put the data into the Tx buffer
        uint8_t data = 0;
        if (uart_driver_get_next_tx_data(&uart1, &data) == 0) {
            INTEnable(INT_SOURCE_UART_TX(uart1.uart), INT_DISABLED);
        }
        UARTSendDataByte(uart1.uart, data);
		INTClearFlag(INT_SOURCE_UART_TX(UART1));
	}
}

