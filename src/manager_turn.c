/**
  ******************************************************************************
  * @file    manager_alarm.c
  * @author  samuel.cifuentes@titoma.com
  * @brief   FSM and MCU interface for the alarm.
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>

#include "manager_turn.h"

/**
 * @addtogroup KIR08_Managers
 * @{
 */

/**
 * @addtogroup alarm_Manager
 * @{
 */

/* Private Defines ------------------------------------------------------------*/
/* Private typedef ------------------------------------------------------------*/
/* Private macro --------------------------------------------------------------*/
#define __TURN_DBG_
#ifdef __TURN_DBG_
#define TURN_print_dbg dbg_print
#else
#define TURN_print_dbg(...)
#endif /* __TURNSTILE_DBG_ */


/* Private Variables declaration ----------------------------------------------*/
/* Private function prototypes ------------------------------------------------*/
/* Functions definition ------------------------------------------------------ */
/**
 * @defgroup alarm_Manager_Driver     alarm Manager Driver Functions
 * @{
 */

static void lock(void *handler)
{
	turn_manager_t *turn = (turn_manager_t *)handler;

}
static void unlock(void *handler)
{
	turn_manager_t *turn = (turn_manager_t *)handler;

}
static void on_error(void *handler)
{
	turn_manager_t *turn = (turn_manager_t *)handler;
}



/**
 * @brief  This function checks for event of the FSM of the alarm manager.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void turn_poll_event(void *handler)
{
    uint8_t buffer[64];
	turn_manager_t *turn = (turn_manager_t *)handler;

	if (fsm_get_event(&turn->fsm) != FSM_EV_NO_EVENT) { // don't overwrite the events
		return;
	}
    uart_driver_read_string(UART1, buffer);
    if (buffer[0]=='#'){
        if (buffer[1]=='C'){
            fsm_set_event(&turn->fsm, TURN_EV_COIN);
        }else{
            fsm_set_event(&turn->fsm, TURN_EV_PUSH);
        }  
    }else{
        fsm_set_event(&turn->fsm, TURN_EV_NO_EVENT);
    }
    
}

/**
  * @brief  initialize the look up table for the buzzer state machine
  *
  * @note   \b lookup_table \b
  *     [ Current state    Event                Event handler     Next state        ]
  *                                                                                 
  *     [ ALARM_ST_IDLE     ALARM_EV_RUN_INIT     on_run_time_init  ALARM_ST_HIGH      ]
  *     [ ALARM_ST_HIGH     ALARM_EV_HIGH_STOP    on_high_stop      ALARM_ST_LOW       ]
  *     [ ALARM_ST_HIGH     ALARM_EV_RUN_STOP     on_run_time_stop  ALARM_ST_IDLE      ]
  *     [ ALARM_ST_LOW      ALARM_EV_HIGH_INIT    on_high_init      ALARM_ST_HIGH      ]
  *     [ ALARM_ST_LOW      ALARM_EV_ACTIVE_STOP  on_active_stop    ALARM_ST_INACTIVE  ]
  *     [ ALARM_ST_LOW      ALARM_EV_RUN_STOP     on_run_time_stop  ALARM_ST_IDLE      ]
  *     [ ALARM_ST_INACTIVE ALARM_EV_HIGH_INIT    on_high_init      ALARM_ST_HIGH      ]
  *     [ ALARM_ST_INACTIVE ALARM_EV_RUN_STOP     on_run_time_stop  ALARM_ST_IDLE      ]
*/
const fsm_on_event_t turn_on_event[TURN_EV_TOTAL][TURN_ST_TOTAL] = {
/* ST \ EV     *//* ERROR            COIN             PUSH               INACTIVE        */
/* ERROR       */  {on_error,        on_error,        on_error},
/* NO_EVENT    */  {NULL,            NULL,            NULL},
/* LOCKED      */  {on_error, unlock, NULL},
/* UNLOCKED    */  {on_error,            unlock,            lock}
};

const turn_states_t turn_next_state[TURN_EV_TOTAL][TURN_ST_TOTAL] = {
/* ST \ EV     *//* ERROR            COIN             PUSH               INACTIVE        */
/* ERROR       */  {TURN_ST_ERROR,        TURN_ST_ERROR,        TURN_ST_ERROR},
/* NO_EVENT    */  {TURN_ST_ERROR,            TURN_ST_ERROR,            TURN_ST_ERROR},
/* LOCKED      */  {TURN_ST_ERROR, TURN_ST_UNLOCKED, TURN_ST_LOCKED},
/* UNLOCKED    */  {TURN_ST_ERROR, TURN_ST_UNLOCKED,     TURN_ST_LOCKED}
};




/**
 * @addtogroup alarm_Manager_interface
 * @{
 */

/**
 * @brief  This function initializes the variables, peripherals and FSM of the alarm manager.
 * @param  alarm: The pointer to the instance of the alarm manager.
 * @retval None.
 */
void turn_manager_init(turn_manager_t *turn)
{
	const fsm_init_t turn_fsm_init = {
		.parent             = turn,
		.poll_event_handler = turn_poll_event,
		.on_event           = (fsm_on_event_t  *)turn_on_event,
		.next_state         = (fsm_state_t *)turn_next_state,
		.max_state          = (fsm_state_t)TURN_ST_TOTAL
	};
	
	fsm_init(&turn->fsm, (fsm_init_t *)&turn_fsm_init);

}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************************ END OF FILE *************************************/
