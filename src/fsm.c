/**
 ******************************************************************************
 * @file    fsm.c
 * @author  samuel.cifuentes@titoma.com
 * @brief   Implementation of a generic Finite State Machine.
 ******************************************************************************
 */

/* Includes -------------------------------------------------------------------*/
#include <fsm.h>
#include <string.h>
#include <stdio.h>


/**
 * @addtogroup Generic_FSM Generic FSM
 * @{
 */

/**
 * @defgroup Generic_FSM_Private_Macro Generic FSM Private Macros
 * @{
 */

/**
 * @brief  It is well known for the arrays in ANSI c that:
 *         **array[i][j] = array[(total_j * i) + j]**.
 * @param  TOTAL_STATE: the total amount of event of the FSM.
 * @param  STATE: The current state.
 * @param  EVENT: The event that was triggered.
 * @retval index: The index for [event][state].
 *
 * Example 1 (total_state = 2): array[event_1][state_1] = array[(2 * 1) + 1] = array[3]
 * |         | state_0  | state_1  |
 * | ------- | -------  | -------- |
 * | event_0 | lookup_0 | lookup_1 |
 * | event_1 | lookup_2 | lookup_3 |
 *
 * Example 2 (total_state = 3): array[event_1][state_2] = array[(3 * 1) + 2] = array[5]
 * |         | state_0  | state_1  | state_2  |
 * | ------- | -------  | -------- | -------- |
 * | event_0 | lookup_0 | lookup_1 | lookup_2 |
 * | event_1 | lookup_3 | lookup_4 | lookup_5 |
 */
#define GET_LOOKUP_TABLE_INDEX(TOTAL_STATE, STATE, EVENT) (TOTAL_STATE * EVENT + STATE)

/** @} */

/**
 * @defgroup Generic_FSM_Functions_Definition Generic FSM Function Definition
 * @{
 */

/**
* @brief  This function initializes a generic fsm for any kind of handler
* @param  fsm: the state machine to be initialized
* @param  fsm_init: An structure containing some external implementation that are needed to run the state machine
* @retval None
*/
void fsm_init(fsm_t *fsm, fsm_init_t *fsm_init)
{
	if (fsm == NULL || fsm_init == NULL) {
		return;
	} else {
		//fsm_dbg("[FSM][%lX]Init\r\n", (uint32_t)fsm);
	}
	fsm->parent     = fsm_init->parent;
	fsm->poll_event = fsm_init->poll_event_handler;

	fsm->lookup.max_state = fsm_init->max_state;
	fsm->lookup.on_event   = fsm_init->on_event;
	fsm->lookup.next_state = fsm_init->next_state;
	
	fsm->state      = FSM_ST_ENTRY;
	fsm->event      = FSM_EV_NO_EVENT;
	fsm->is_running = 0;
}

/**
* @brief  This function runs a generic state machine checking if any event occurs,
*         executing the event handler and setting the next state if so.
* @param  fsm: the state machine to be run
* @retval None
*/
void fsm_run(fsm_t* fsm)
{
	fsm->poll_event(fsm->parent);
	if (fsm->event != FSM_EV_NO_EVENT) {
		uint8_t lookup_index = GET_LOOKUP_TABLE_INDEX(fsm->lookup.max_state, fsm->state, fsm->event);
		fsm->event = FSM_EV_NO_EVENT;
		if (fsm->lookup.on_event[lookup_index] != NULL) {
			fsm->lookup.on_event[lookup_index](fsm->parent);
			fsm->state = fsm->lookup.next_state[lookup_index];
//			dbg_print("State: %X\r\n", fsm->state);
		}
	}
}

/** @} */

/** @} */

/********************************* END OF FILE *********************************/
