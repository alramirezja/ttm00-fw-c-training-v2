/**
  ******************************************************************************
  * @file     ring_buffer.c
  * @author   samuel.cifuentes@titoma.com
  * @brief    Ring buffer variables and APIs.
  ******************************************************************************
*/

/* Includes -------------------------------------------------------------------*/
#include "ring_buffer.h"
#include <stdbool.h>


/* Private Defines ------------------------------------------------------------*/

/* Private typedef ------------------------------------------------------------*/
/* Private macro --------------------------------------------------------------*/
//#define __RING_BUFFER_DBG_
#ifdef __RING_BUFFER_DBG_
#define ring_buffer_dbg dbg_print
#else
#define ring_buffer_dbg(...)
#endif /* __SPIRIT1_APP_DBG_ */

/* Private Variables declaration ----------------------------------------------*/
/* Private function prototypes ------------------------------------------------*/
/* Functions definition ------------------------------------------------------ */

/**
 * @brief  This function initializes a ring buffer structure based on some
 *         given parameters.
 * @param  ring_buffer: pointer to the structure to be initialized.
 * @param  p_buffer: physical memory address to save the data.
 * @param  t_size: length of the memory allocated for the buffer.
 * @param  on_full_buff: pointer to the handler to be called when the buffer is full.
 * @retval 0: If initialization failed.
 * @retval id: otherwise.
*/
uint8_t ring_buffer_init(volatile ring_buffer_t *ring_buffer, uint8_t *p_buffer, size_t t_size)
{
	if(p_buffer == NULL || t_size == 0) {
		return 0;
	}
	ring_buffer->buffer = p_buffer;
	ring_buffer->max = t_size;
	ring_buffer_reset(ring_buffer);
	return 1;
}

/**
 * @brief   To check the ring buffer current size.
 * @param   ring_buffer: pointer to the structure to query.
 * @retval  size of the current available data.
*/
size_t ring_buffer_size(volatile ring_buffer_t *ring_buffer)
{
	size_t size;

	if(ring_buffer->full == 0) {
		if (ring_buffer->head >= ring_buffer->tail) {
			size = (ring_buffer->head - ring_buffer->tail);
		} else {
			size = (ring_buffer->max + ring_buffer->head - ring_buffer->tail);
		}
	} else {
		size = ring_buffer->max;
	}

	return size;
}

/**
 * @brief   To add data to the ring buffer. The routine calls a callback when a buffer
 *          gets full.
 * @param   ring_buffer: pointer to the desired structure where to add.
 * @param   t_data: data to add to the buffer.
 * @retval  None
*/
void ring_buffer_put(volatile ring_buffer_t *ring_buffer, uint8_t t_data)
{
	ring_buffer->buffer[ring_buffer->head] = t_data;
	ring_buffer->head = (ring_buffer->head + 1) % ring_buffer->max;
	
	if (ring_buffer->full != 0) {
		ring_buffer->tail = (ring_buffer->tail + 1) % ring_buffer->max;
	}
	
	if (ring_buffer->head == ring_buffer->tail) {
		ring_buffer->full = 1;
	}
}

/**
 * @brief   To get a data byte if available.
 * @param   ring_buffer: pointer to the structure from to get.
 * @param   p_data: pointer to the byte to save the data value.
 * @retval  0: data no available.
 * @retval  1: data available.
*/
uint8_t ring_buffer_get(volatile ring_buffer_t *ring_buffer, uint8_t *p_data)
{
	if (ring_buffer_empty(ring_buffer) == 0) {
		*p_data = ring_buffer->buffer[ring_buffer->tail];
		ring_buffer->tail = (ring_buffer->tail + 1) % ring_buffer->max;
		ring_buffer->full = 0;

		return 1;
	}

	return 0;
}

/**
 * @brief  This function returns the pointer to the tail of the ring buffer.
 * @param  ring_buffer: pointer to the desired structure where to query the data.
 * @retval None
 * */
uint8_t *ring_buffer_fetch(volatile ring_buffer_t *ring_buffer)
{
	return &ring_buffer->buffer[ring_buffer->tail];
}

/********************************* END OF FILE *********************************/
