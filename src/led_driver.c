#include <plib.h>

#include "led_driver.h"

#define LED_R BIT_10
#define LED_G BIT_3
#define LED_B BIT_7
#define LED_1 BIT_4
#define LED_2 BIT_6
#define LED_3 BIT_7

static uint32_t led_driver_get_bit(led_t led)
{
    switch (led) {
        case led_r:
            return LED_R;
        case led_g:
            return LED_G;
        case led_b:
            return LED_B;
        case led_1:
            return LED_1;
        case led_2:
            return LED_2;
        case led_3:
            return LED_3;
        default:
            /*!\ TODO: Add a mechanism to prevent errors */
            return 0;
    }
}

void led_driver_init(void)
{
    mPORTBSetPinsDigitalOut(LED_R | LED_G | LED_B);
    mPORTBSetBits(LED_R | LED_G | LED_B);
    
    mPORTESetPinsDigitalOut(LED_1 | LED_2 | LED_3);
    mPORTEClearBits(LED_1 | LED_2 | LED_3);
}

void led_driver_set(led_t led)
{
    uint32_t led_bit = led_driver_get_bit(led);
    switch (led) {
        case led_r:
        case led_g:
        case led_b:
            mPORTBSetBits(led_bit);
            break;
        case led_1:
        case led_2:
        case led_3:
            mPORTESetBits(led_bit);
            break;
        default:
            break;
    }
}

void led_driver_clr(led_t led)
{
    uint32_t led_bit = led_driver_get_bit(led);
    switch (led) {
        case led_r:
        case led_g:
        case led_b:
            mPORTBClearBits(led_bit);
            break;
        case led_1:
        case led_2:
        case led_3:
            mPORTEClearBits(led_bit);
            break;
        default:
            break;
    }
}

void led_driver_toggle(led_t led)
{
    uint32_t led_bit = led_driver_get_bit(led);
    switch (led) {
        case led_r:
        case led_g:
        case led_b:
            mPORTBToggleBits(led_bit);
            break;
        case led_1:
        case led_2:
        case led_3:
            mPORTEToggleBits(led_bit);
            
            break;
        default:
            break;
    }
}
