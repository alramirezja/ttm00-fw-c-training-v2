#include <string.h>
#include <stdint.h>
#include "plib.h"

#include "uart_driver.h"
#include "main.h"
char buffer[64];
uint8_t ind=0;
/**
  * @brief  This function initializes the uart driver variables and configures the 
  *         uart port.
  * @arg    uart_driver: The pointer to the uart  structure to be used for future transactions.
  * @arg    config: The pointer to the structure that contains the initalization info for the uart.
  * @retval None.
*/
void uart_driver_init(uart_driver_t *uart_driver, uart_driver_config_t *config) 
{
    memset(uart_driver, 0, sizeof(uart_driver_t));
    
    uart_driver->uart = config->uart;
    uart_driver->rx.buffer = config->rx_buffer;
    uart_driver->rx.max_length = config->rx_max_length;
    uart_driver->tx.buffer = config->tx_buffer;
    uart_driver->tx.max_length = config->tx_max_length;
    
    //UART Configure
    UARTConfigure(uart_driver->uart, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(uart_driver->uart, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(uart_driver->uart, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(uart_driver->uart, PBCLK, config->baud_rate);
    UARTEnable(uart_driver->uart, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
    
//    // Configure UART RX Interrupt
//    INTEnable(INT_SOURCE_UART_RX(uart_driver->uart), INT_ENABLED);
    INTSetVectorPriority(INT_VECTOR_UART(uart_driver->uart), INT_PRIORITY_LEVEL_2);
    INTSetVectorSubPriority(INT_VECTOR_UART(uart_driver->uart), INT_SUB_PRIORITY_LEVEL_0);
}