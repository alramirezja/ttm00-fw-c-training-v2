#include <string.h>
#include <stdint.h>
#include "plib.h"

#include "uart_driver.h"
#include "main.h"
char buffer[64];
uint8_t ind=0;
/**
  * @brief  This function initializes the uart driver variables and configures the 
  *         uart port.
  * @arg    uart_driver: The pointer to the uart  structure to be used for future transactions.
  * @arg    config: The pointer to the structure that contains the initalization info for the uart.
  * @retval None.
*/
void uart_driver_init(uart_driver_t *uart_driver, uart_driver_config_t *config) 
{
    memset(uart_driver, 0, sizeof(uart_driver_t));
    
    uart_driver->uart = config->uart;
    uart_driver->rx.buffer = config->rx_buffer;
    uart_driver->rx.max_length = config->rx_max_length;
    uart_driver->tx.buffer = config->tx_buffer;
    uart_driver->tx.max_length = config->tx_max_length;
    
    //UART Configure
    UARTConfigure(uart_driver->uart, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(uart_driver->uart, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(uart_driver->uart, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(uart_driver->uart, PBCLK, config->baud_rate);
    UARTEnable(uart_driver->uart, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
    
//    // Configure UART RX Interrupt
//    INTEnable(INT_SOURCE_UART_RX(uart_driver->uart), INT_ENABLED);
    INTSetVectorPriority(INT_VECTOR_UART(uart_driver->uart), INT_PRIORITY_LEVEL_2);
    INTSetVectorSubPriority(INT_VECTOR_UART(uart_driver->uart), INT_SUB_PRIORITY_LEVEL_0);
}

/**
  * @brief  This function writes a char in the specified uart port.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @arg    data: The byte to be transmitted.
  * @retval None.
*/
void uart_driver_write_char(uart_driver_t *uart_driver, uint8_t data)
{
    // Wait till the Tx buffer is empty
    while(!UARTTransmitterIsReady(uart_driver->uart)); 
    // Put the data into the Tx buffer
    UARTSendDataByte(uart_driver->uart, data);
    // Wait for the data to be completely transmitted
    while(!UARTTransmissionHasCompleted(uart_driver->uart));
}

/**
  * @brief  This function writes a string in the specified uart port.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @arg    data: The string to be transmitted.
  * @retval None.
*/
void uart_driver_write_string(uart_driver_t *uart_driver, uint8_t *data)
{
    if ((uart_driver == NULL) || (data == NULL)) {
        return;
    }
    while (*data != '\0') {
        uart_driver_write_char(uart_driver, *(data++));
    }
}
/**
  * @brief  This function writes a determined number of characters in the specified uart port.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @arg    data: The string to be transmitted.
  * @retval None.
*/
void uart_driver_write_packet(uart_driver_t *uart_driver, uint8_t *data)
{
    uint8_t i;
    if ((uart_driver == NULL)|| (data == NULL)) {
        return;
    }
    for (i=0; i<=7;i++){
        uart_driver_write_char(uart_driver, *(data++));
    }
}
/**
  * @brief  This function reads a byte from the specified uart port in blocking mode.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @retval data: The byte received in the uart port.
*/
uint8_t uart_driver_read_char(uart_driver_t *uart_driver)
{
    /* Wait for a data byte to arrive */
    while (UARTReceivedDataIsAvailable(uart_driver->uart) == 0);
    
    /* Read data byte */
    uint8_t data = UARTGetDataByte(uart_driver->uart);
    
    return data;
}
/**
  * @brief  This function reads multiple bytes from the specified uart port in blocking mode.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @retval buffer: The bytes received in the uart port.
*/
char uart_driver_read_string(uart_driver_t *uart_driver)
{
    while (IFS1bits.U1RXIF == 1) {
        buffer[ind]=uart_driver_read_char(uart_driver);
        ind++;
        IFS1bits.U1RXIF = 0;
    }
    if (ind>0){
        return *buffer;
        ind=0;
    }
    
    
}
/**
  * @brief  This function reads 8 bytes from the specified uart port in blocking mode.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @retval buffer: The bytez received in the uart port.
*/
char uart_driver_read_packet(uart_driver_t *uart_driver)
{
    while ((IFS1bits.U1RXIF == 1)&&(ind<8)) {
        buffer[ind]=uart_driver_read_char(uart_driver);
        ind++;
        IFS1bits.U1RXIF = 0;
    }
    if (ind==8){
        uart_driver_write_packet(uart_driver,buffer);
        ind=0;
    }
    return *buffer;
}
/**
  * @brief  This function writes a packet in the specified uart port in IT mode, by 
  *         adding the data to an output buffer and enabling the transmission interrupt.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @arg    data: The packet to be transmitted.
  * @arg    len: The length of the packet to be transmitted.
  * @retval None.
*/

void uart_driver_write_IT(uart_driver_t *uart_driver, uint8_t *data, uint16_t len)
{
    uart_driver->bytes_to_send = len;
    uart_driver->tx.counter = 0;
    memcpy(uart_driver->tx.buffer, data, uart_driver->bytes_to_send);
    INTEnable(INT_SOURCE_UART_TX(uart_driver->uart), INT_ENABLED);
}
/**
  * @brief  This function reads a packet in the specified uart port in IT mode, depending on the mode selected,
  * it will stop reading when the char readed is a specific character or the packet has a specific size.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @arg    end: Size or last readed character of the packet.
  * @arg    flag: Mode of work, 1 (one) to stop reading when the packethas a specific size 
  * and 0 (zero) to stop reading when an specific character is detected.
  * @retval None.
*/
void uart_driver_read_IT(uart_driver_t *uart_driver, uint8_t end,uint8_t flag)
{
    uart_driver->rx_unread=end;
    uart_driver->rx_flag=flag;
    INTEnable(INT_SOURCE_UART_RX(uart_driver->uart), INT_ENABLED);
}
/**
  * @brief  This function gets the next byte to be transmitted by the uart port. This data 
  *         should be put in the output register in an external routine like the interruption handler.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @arg    data: The string to be transmitted.
  * @retval None.
*/
uint8_t uart_driver_get_next_tx_data(uart_driver_t *uart_driver, uint8_t *data) 
{
    uint8_t data_available = 0;
    *data = uart_driver->tx.buffer[uart_driver->tx.counter++];
    if (uart_driver->tx.counter >= uart_driver->bytes_to_send) {
        return 0;
    }
    return 1;
}
/**
  * @brief  This function read the character in the port rx until the message readed has a specific size
  * or the readed character is one specified to end the reception.
  * @arg    uart_driver: The pointer to the uart structure. NOTE: shall be previously initialized.
  * @retval Buffer: Message received.
*/
uint8_t uart_driver_receive_rx_data(uart_driver_t *uart_driver) {  
    uint8_t flag=uart_driver->rx_flag;
    uint8_t end=uart_driver->rx_unread;
    if (flag){
        while (IFS1bits.U1RXIF == 1) {
            if (ind<end){
                buffer[ind]=uart_driver_read_char(uart_driver);
                ind++;
            }
            IFS1bits.U1RXIF = 0;
        }
    } else {
        while (IFS1bits.U1RXIF == 1){
            if(buffer[ind-1]!=end) {
                buffer[ind]=uart_driver_read_char(uart_driver);
                ind++;
            }
            IFS1bits.U1RXIF = 0;       
    }
        //buffer[ind-1]='\0';
    }
    if ((buffer[ind-1]==end)||(ind==end)){
        uart_driver_write_string(uart_driver,buffer);
        ind=0;
        INTEnable(INT_SOURCE_UART_RX(UART1), INT_DISABLED);
    }

}
