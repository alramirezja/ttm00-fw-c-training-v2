/**
  ******************************************************************************
  * @file    manager_alarm.c
  * @author  samuel.cifuentes@titoma.com
  * @brief   FSM and MCU interface for the alarm.
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>

#include "manager_alarm.h"

/**
 * @addtogroup KIR08_Managers
 * @{
 */

/**
 * @addtogroup alarm_Manager
 * @{
 */

/* Private Defines ------------------------------------------------------------*/
/* Private typedef ------------------------------------------------------------*/
/* Private macro --------------------------------------------------------------*/
#define __ALARM_DBG_
#ifdef __ALARM_DBG_
#define alarm_print_dbg dbg_print
#else
#define alarm_print_dbg(...)
#endif /* __ALARM_DBG_ */


/* Private Variables declaration ----------------------------------------------*/
/* Private function prototypes ------------------------------------------------*/
/* Functions definition ------------------------------------------------------ */
/**
 * @defgroup alarm_Manager_Driver     alarm Manager Driver Functions
 * @{
 */

/**
 * @brief  This function initializes the variables and peripherals used by the alarm manager.
 * @param  alarm: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void alarm_mcu_interface_init(alarm_manager_t *alarm, led_t led)
{
	alarm->mcu_intf.led = led;
}

/**
 * @}
 */


/**
 * @addtogroup alarm_Manager_Exported
 * @{
 */

/**
 * @brief  This function sets and starts a given pattern for the alarm.
 * @param  alarm: The pointer to the instance of the alarm manager.
 * @retval None.
 * @example frequency: 4Hz, duty_cycle: 50, on_time: 500, off_time: 500, pattern_time: 3000
 *                  |--------------------- 3 s ---------------------|
 *                  |----- 1 s -----| 0.5 s | 0.5 s |      1 s      |
 *          ..._____--__--__________--__--__________--__--________________...
 */
void alarm_set_pattern(alarm_manager_t *alarm, alarm_config_t *t_config)
{
	memcpy(&alarm->config, t_config, sizeof(alarm_config_t));

	alarm->time.high = (1000 / alarm->config.low_frequency) * ((alarm->config.low_duty_cycle / 100.0));
	alarm->time.low = (1000 / alarm->config.low_frequency) - alarm->time.high;
	alarm->time.active = alarm->config.on_time;
	alarm->time.inactive = alarm->config.off_time;
	
	alarm->time.pattern_stop = system_get_tick() + alarm->config.pattern_time;
	alarm->time.high_init = 0xFFFFFFFF;
	alarm->time.high_stop = 0xFFFFFFFF;
	alarm->time.active_stop = 0xFFFFFFFF;
	
	fsm_set_event(&alarm->fsm, ALARM_EV_RUN_INIT);
	alarm->fsm.is_running = 1;
}

void alarm_manager_test(alarm_manager_t *alarm_1, alarm_manager_t *alarm_2)
{
    alarm_config_t test_config_1 = {
        .pattern_time = 10000, // 5s
        .low_frequency = 8,  // 4 beeps per second
        .low_duty_cycle = 30,
        .on_time = 2000,
        .off_time = 2000
    };
    alarm_config_t test_config_2 = {
        .pattern_time = 20000, // 5s
        .low_frequency = 4,  // 4 beeps per second
        .low_duty_cycle = 80,
        .on_time = 2000,
        .off_time = 1000
    };
    alarm_set_pattern(alarm_1, &test_config_1);
    alarm_set_pattern(alarm_2, &test_config_2);
}

/**
 * @brief  This function triggers an event that stops the execution of the pattern for the alarm.
 * @param  alarm: The pointer to the instance of the buzzer manager.
 * @retval None.
 */
void alarm_stop_pattern(alarm_manager_t *alarm)
{
	alarm->time.pattern_stop = system_get_tick();
	//fsm_set_event(&alarm->fsm, ALARM_EV_RUN_STOP);
}

/**
 * @}
 */

/**
 * @defgroup alarm_Manager_FSM     alarm Manager FSM Functions
 * @{
 */

/**
 * @brief  This function is executed at the beginning of the pattern time.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void on_pattern_init(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;
	alarm_print_dbg("[ALARM]Init\r\n");

	alarm_enable(alarm);

	alarm->time.high_stop = system_get_tick() + alarm->time.high;
	alarm->time.high_init = alarm->time.high_stop + alarm->time.low; //debug
	
	alarm->time.active_stop = system_get_tick() + alarm->time.active;
}

/**
 * @brief  This function is executed at the end of the pattern time.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void on_pattern_stop(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;
	alarm_print_dbg("[ALARM]Finish\r\n");

	alarm_disable(alarm);
	
	alarm->fsm.is_running = 0;
}

/**
 * @brief  This function is executed at the end of each ring.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void on_high_stop(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;
	alarm_print_dbg("[ALARM]Low\r\n");

	alarm_disable(alarm);

	alarm->time.high_init = system_get_tick() + alarm->time.low;
	alarm->time.high_stop = alarm->time.high_init + alarm->time.high;
}

/**
 * @brief  This function is executed at the beginning of each ring.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void on_high_init(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;
	alarm_print_dbg("[ALARM]High\r\n");

	alarm_enable(alarm);
	
	alarm->time.high_stop = system_get_tick() + alarm->time.high;
	alarm->time.high_init = alarm->time.high_stop + alarm->time.low;
}

/**
 * @brief  This function is executed when the active cycle of the alarm's signal is elapsed.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void on_active_stop(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;
	alarm_print_dbg("[ALARM]Inactive\r\n");

	alarm_disable(alarm);
	
	alarm->time.high_init = system_get_tick() + alarm->time.inactive;
	alarm->time.high_stop = alarm->time.high_init + alarm->time.high;
	
	alarm->time.active_stop = alarm->time.high_init + alarm->time.active;
}

/**
 * @brief  This function is executed when an error is detected in FSM of the alarm manager.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void on_error(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;
	alarm_print_dbg("[ALARM]Error\r\n");
	UNUSED(alarm);
}

/**
 * @brief  This function checks for event of the FSM of the alarm manager.
 * @param  handler: The pointer to the instance of the alarm manager.
 * @retval None.
 */
static void alarm_poll_event(void *handler)
{
	alarm_manager_t *alarm = (alarm_manager_t *)handler;

	if (fsm_get_event(&alarm->fsm) != FSM_EV_NO_EVENT) { // don't overwrite the events
		return;
	}

	uint32_t current_tick = system_get_tick();
	
	if (current_tick < alarm->time.pattern_stop) {
		if (alarm->time.high_init < current_tick) {
			fsm_set_event(&alarm->fsm, ALARM_EV_HIGH_INIT);
		}
		if (alarm->time.high_stop < current_tick) {
			fsm_set_event(&alarm->fsm, ALARM_EV_HIGH_STOP);
		}
		if (alarm->time.active_stop < current_tick) {
			fsm_set_event(&alarm->fsm, ALARM_EV_ACTIVE_STOP);
		}
	}else {
		fsm_set_event(&alarm->fsm, ALARM_EV_RUN_STOP);
	}
}

/**
  * @brief  initialize the look up table for the buzzer state machine
  *
  * @note   \b lookup_table \b
  *     [ Current state    Event                Event handler     Next state        ]
  *                                                                                 
  *     [ ALARM_ST_IDLE     ALARM_EV_RUN_INIT     on_run_time_init  ALARM_ST_HIGH      ]
  *     [ ALARM_ST_HIGH     ALARM_EV_HIGH_STOP    on_high_stop      ALARM_ST_LOW       ]
  *     [ ALARM_ST_HIGH     ALARM_EV_RUN_STOP     on_run_time_stop  ALARM_ST_IDLE      ]
  *     [ ALARM_ST_LOW      ALARM_EV_HIGH_INIT    on_high_init      ALARM_ST_HIGH      ]
  *     [ ALARM_ST_LOW      ALARM_EV_ACTIVE_STOP  on_active_stop    ALARM_ST_INACTIVE  ]
  *     [ ALARM_ST_LOW      ALARM_EV_RUN_STOP     on_run_time_stop  ALARM_ST_IDLE      ]
  *     [ ALARM_ST_INACTIVE ALARM_EV_HIGH_INIT    on_high_init      ALARM_ST_HIGH      ]
  *     [ ALARM_ST_INACTIVE ALARM_EV_RUN_STOP     on_run_time_stop  ALARM_ST_IDLE      ]
*/
const fsm_on_event_t alarm_on_event[ALARM_EV_TOTAL][ALARM_ST_TOTAL] = {
/* ST \ EV     *//* ERROR            IDLE             HIGH             LOW              INACTIVE        */
/* ERROR       */  {on_error,        on_error,        on_error,        on_error,        on_error        },
/* NO_EVENT    */  {NULL,            NULL,            NULL,            NULL,            NULL            },
/* RUN_INIT    */  {on_pattern_init, on_pattern_init, on_pattern_init, on_pattern_init, on_pattern_init },
/* HIGH_INIT   */  {NULL,            NULL,            NULL,            on_high_init,    on_high_init    },
/* HIGH_STOP   */  {NULL,            NULL,            on_high_stop,    NULL,            NULL            },
/* ACTIVE_STOP */  {NULL,            NULL,            NULL,            on_active_stop,  NULL            },
/* RUN_STOP    */  {on_pattern_stop, on_pattern_stop, on_pattern_stop, on_pattern_stop, on_pattern_stop }
};

const alarm_states_t alarm_next_state[ALARM_EV_TOTAL][ALARM_ST_TOTAL] = {
/* ST \ EV     *//* ERROR           IDLE            HIGH            LOW                INACTIVE */
/* ERROR       */  {ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_ERROR,    ALARM_ST_ERROR},
/* NO_EVENT    */  {ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_ERROR,    ALARM_ST_ERROR},
/* RUN_INIT    */  {ALARM_ST_HIGH,  ALARM_ST_HIGH,  ALARM_ST_HIGH,  ALARM_ST_HIGH,     ALARM_ST_HIGH},
/* HIGH_INIT   */  {ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_HIGH,     ALARM_ST_HIGH},
/* HIGH_STOP   */  {ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_LOW,   ALARM_ST_ERROR,    ALARM_ST_ERROR},
/* ACTIVE_STOP */  {ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_ERROR, ALARM_ST_INACTIVE, ALARM_ST_ERROR},
/* RUN_STOP    */  {ALARM_ST_IDLE,  ALARM_ST_IDLE,  ALARM_ST_IDLE,  ALARM_ST_IDLE,     ALARM_ST_IDLE}
};

/**
 * @}
 */


/**
 * @addtogroup alarm_Manager_interface
 * @{
 */

/**
 * @brief  This function initializes the variables, peripherals and FSM of the alarm manager.
 * @param  alarm: The pointer to the instance of the alarm manager.
 * @retval None.
 */
void alarm_manager_init(alarm_manager_t *alarm, led_t led)
{
	const fsm_init_t alarm_fsm_init = {
		.parent             = alarm,
		.poll_event_handler = alarm_poll_event,
		.on_event           = (fsm_on_event_t  *)alarm_on_event,
		.next_state         = (fsm_state_t *)alarm_next_state,
		.max_state          = (fsm_state_t)ALARM_ST_TOTAL
	};
	
	fsm_init(&alarm->fsm, (fsm_init_t *)&alarm_fsm_init);
	alarm_mcu_interface_init(alarm, led);
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************************ END OF FILE *************************************/
