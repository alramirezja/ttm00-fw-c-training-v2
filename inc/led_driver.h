/* 
 * File:   led_driver.h
 * Author: user8
 *
 * Created on July 22, 2021, 2:23 AM
 */

#ifndef LED_DRIVER_H
#define	LED_DRIVER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum led_ {
    led_r = 0x00,
    led_g,
    led_b,
    led_1,
    led_2,
    led_3,
} led_t;


void led_driver_init(void);
void led_driver_set(led_t);
void led_driver_clr(led_t);
void led_driver_toggle(led_t);

#ifdef	__cplusplus
}
#endif

#endif	/* LED_DRIVER_H */

