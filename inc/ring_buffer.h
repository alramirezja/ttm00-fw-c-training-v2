/**
  ******************************************************************************
  * @file   ring_buffer.h
  * @author samuel.cifuentes@titoma.com
  * @brief  Ring buffer variables and APIs.
  ******************************************************************************
  */
	
/* Define to prevent recursive inclusion -------------------------------------*/\
#ifndef __RING_BUFFER_H
#define __RING_BUFFER_H

/** Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>

/* Exported defines ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @struct ring_buffer_
 * @brief  Ring buffer structure definition
 * */
typedef struct ring_buffer_  {
    uint8_t  *buffer; /*!< The pointer to the physical space in memory for the data */
    size_t   head;    /*!< The pointer to the last entered data */
    size_t   tail;    /*!< The pointer to the last requested data */
    size_t   max;     /*!< Maximum capacity of the buffer. Used to avoid memory access errors */
    uint8_t  full;    /*!< Flag to record when the buffer gets full */
    
} ring_buffer_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

uint8_t ring_buffer_init(volatile ring_buffer_t *, uint8_t *, size_t);
size_t ring_buffer_size(volatile ring_buffer_t *);
void ring_buffer_put(volatile ring_buffer_t *, uint8_t );
uint8_t ring_buffer_get(volatile ring_buffer_t *, uint8_t *);
uint8_t *ring_buffer_fetch(volatile ring_buffer_t *);


/**
 * @brief   this function resets the structure's management variables.
 * @param   ring_buffer: pointer to the structure to be reset.
 * @retval  None.
*/
static inline void ring_buffer_reset(volatile ring_buffer_t *ring_buffer)
{
	ring_buffer->head = 0;
	ring_buffer->tail = 0;
	ring_buffer->full = 0;
}

/**
 * @brief   To check if the ring buffer is  full.
 * @param   ring_buffer: pointer to the structure to query.
 * @retval  0: Not full
 * @retval  1: Full
*/
static inline uint8_t ring_buffer_full(volatile ring_buffer_t *ring_buffer)
{
	return ring_buffer->full;
}

/**
 * @brief   To check if the ring buffer is  empty.
 * @param   ring_buffer: pointer to the structure to query.
 * @retval  0: Not empty
 * @retval  1: Empty
*/
static inline uint8_t ring_buffer_empty(volatile ring_buffer_t *ring_buffer)
{
	return ((ring_buffer->full == 0) && (ring_buffer->head == ring_buffer->tail));
}

/**
 * @brief   To check the ring buffer max capacity.
 * @param   ring_buffer: pointer to the structure to query.
 * @retval  Value of the buffer's length given in the initialization.
*/
static inline size_t ring_buffer_capacity(volatile ring_buffer_t *ring_buffer)
{
		return ring_buffer->max;
}


#endif /* __RING_BUFFER_H */
/************************************ END OF FILE *************************************/
