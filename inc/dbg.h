/* 
 * File:   dbg.h
 * Author: user8
 *
 * Created on July 22, 2021, 1:19 AM
 */

#ifndef DBG_H
#define	DBG_H

#ifdef	__cplusplus
extern "C" {
#endif

#define __DEBUG_
#ifdef __DEBUG_
#define dbg_print printf
#endif /* __DEBUG_ */

#ifdef	__cplusplus
}
#endif

#endif	/* DBG_H */

