/**
  ******************************************************************************
  * @file   manager_alarm.h
  * @author samuel.cifuentes@titoma.com
  * @brief  Functions and types for the alarm management.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TURNSTILE_H_
#define __TURNSTILE_H_

/* Includes -------------------------------------------------------------------*/
#include <stdint.h>

#include "main.h"
#include "fsm.h"
#include "led_driver.h"

/* Exported constants --------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @defgroup alarm_Manager alarm
 * @{
 */

typedef enum turn_states_ {
	TURN_ST_ERROR = 0,
	TURN_ST_LOCKED,       
	TURN_ST_UNLOCKED,
    TURN_ST_TOTAL
            
	
}turn_states_t;

typedef enum turn_events_ {
	TURN_EV_ERROR = 0,
	TURN_EV_NO_EVENT, // 1
	TURN_EV_COIN,
    TURN_EV_PUSH,
    TURN_EV_TOTAL        
	
}turn_events_t;



typedef struct turn_uart_ {
	UART_MODULE   uart;

} turn_uart_t;

typedef struct turn_manager_ {
	turn_uart_t uart_a;
	fsm_t          fsm;

	
} turn_manager_t;

/**
 * @defgroup alarm_Manager_Exported          alarm Manager Exported Functions
 * @{
 */





/**
 * @}
 */

/**
 * @defgroup alarm_Manager_interface     alarm Manager Interface Functions
 * @{
 */

void turn_manager_init(turn_manager_t *);


/**
 * @}
 */

/**
 * @}
 */


#endif /* __ALARM_H_ */

/********************************* END OF FILE *********************************/
