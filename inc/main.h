/* 
 * File:   main.h
 * Author: user8
 *
 * Created on July 9, 2021, 5:47 PM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <xc.h>
#include <plib.h>
    
#include "uart_driver.h"

#define _XTAL_FREQ       20000000
#define SYS_FREQ         96000000
#define PBCLK_FREQUENCY  (96 * 1000 * 1000)
    
#define FPB_DIV 2
#define PBCLK (SYS_FREQ / FPB_DIV)
    
//  @96MHz, CoreTimer ticks 48 million times per second. Loading a 48,000 value means it overloads 1000 times per second (1mS period)    
#define CORE_TICK_RATE  (SYS_FREQ/2/1000) // 1ms
    
extern volatile uint32_t sys_tick;

#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

