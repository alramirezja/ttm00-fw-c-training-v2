/* 
 * File:   uart.h
 * Author: user8
 *
 * Created on July 19, 2021, 2:16 AM
 */
#include <stdint.h>
#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif
    
typedef struct uart_buffer_ {
    uint8_t  *buffer;
    uint16_t max_length;
    uint16_t counter;
    
} uart_buffer_t;

typedef struct uart_driver_config_ {
    UART_MODULE   uart;
    uint32_t baud_rate;
    uint8_t *rx_buffer;
    uint8_t *tx_buffer;
    uint8_t rx_max_length;
    uint8_t tx_max_length;
    
} uart_driver_config_t;

typedef struct uart_driver_ {
    UART_MODULE   uart;
    uart_buffer_t rx;
    uart_buffer_t tx;
    uint8_t       rx_unread;
    uint16_t      bytes_to_send;
    uint8_t       rx_flag;  
    
} uart_driver_t;


void uart_driver_init(uart_driver_t *, uart_driver_config_t *) ;
void uart_driver_write_char(uart_driver_t *, uint8_t);
void uart_driver_write_string(uart_driver_t *, uint8_t *);
void uart_driver_write_packet(uart_driver_t *, uint8_t *);
uint8_t uart_driver_read_char(uart_driver_t *);
char uart_driver_read_string(uart_driver_t *);
char uart_driver_read_packet(uart_driver_t *);
void uart_driver_read_IT(uart_driver_t * , uint8_t ,uint8_t);
void uart_driver_write_IT(uart_driver_t * , uint8_t * ,uint16_t);
uint8_t uart_driver_get_next_tx_data(uart_driver_t *, uint8_t *);
uint8_t uart_driver_receive_rx_data(uart_driver_t *);

#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

