/**
  ******************************************************************************
  * @file    fsm.h
  * @author  samuel.cifuentes@titoma.com
  * @brief   Header for the generic Finite State Machine module.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FSM_
#define __FSM_
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>

/**
 * @defgroup Generic_FSM Generic FSM
 * @brief This group contains the function to handle a Generic FSM from any manager.
 * @{
 */

/**
 * @defgroup Generic_FSM_Public_Macro Generic FSM Public Macros
 * @{
 */

/**
 * @brief This macro is used for debugging purposes.
 * */
#define __DBG_FSM_
#ifdef __DBG_FSM_
#include "dbg.h"
#define fsm_dbg dbg_print
#else
#define fsm_dbg(...)
#endif /* __DBG_FSM_ */

#ifdef USE_HAL_DRIVER
#include "main.h"
#define system_get_tick HAL_GetTick
#endif /* USE_HAL_DRIVER */

/** @} */

/**
 * @defgroup Generic_FSM_Exported_Types Generic FSM Exported Types
 * @{
 */

/**
 * @brief The generic states for any FSM.
 * */
typedef enum fsm_generic_state_ {
	FSM_ST_ERROR = 0, /*!< Error detection state (Mandatory) */
	FSM_ST_ENTRY,     /*!< Entry point and usually the default state (Mandatory) */
	FSM_ST_EXIT,      /*!< Last state before going to default (Suggested) */
	FSM_ST_TOTAL      /*!< The total of states (Suggested) */
	
} fsm_state_t;

/**
 * @brief The generic events for any FSM.
 * */
typedef enum fsm_generic_event_ {
	FSM_EV_ERROR = 0, /*!< Error detection event (Mandatory) */
	FSM_EV_NO_EVENT,  /*!< Value of the event variable when not event has happened (Mandatory) */
	FSM_EV_TOTAL      /*!< Total events for the system (Suggested) */
	
} fsm_event_t;

/**
 * @brief This type is used to generate the on event lookup table
 * 
*/
typedef void (*fsm_on_event_t)(void *);

/**
 * @brief The routine and next state when an event is triggered in a valid state.
 * */
typedef struct fsm_lookup_ {
	fsm_on_event_t  *on_event;  /*!< Lookup table of the routine to execute when an event is triggered */
	fsm_state_t *next_state;           /*!< lookup table of the next state after an event is triggered */	
	fsm_state_t max_state;            /*!< States quantity. Used to calculate the positions in the lookup tables */

} fsm_lookup_t;

/**
 * @brief The structure to handle any FSM.
 * */
typedef struct fsm_ {
	void         *parent;               /*!< Object where the FSM is used in (Mandatory) */

	void         (*poll_event)(void *); /*!< Routine to check the events by polling */
	fsm_lookup_t lookup;
	
	fsm_state_t  state;                 /*!< The current state */
	fsm_event_t  event;                 /*!< Last event registered */
	uint8_t      is_running;            /*!< This flag can be used to avoid running the FSM while idle */

} fsm_t;

/**
 * @brief The structure to initialize any FSM.
 * */
typedef struct fsm_init_ {
	void        *parent;                       /*!< Mandatory */
	void        (*poll_event_handler)(void *); /*!< Optional  */

	fsm_on_event_t  *on_event;          /*!< Mandatory */
	fsm_state_t *next_state;                   /*!< Mandatory */
	fsm_state_t max_state;                     /*!< Mandatory */

} fsm_init_t;

/** @} */

/**
 * @defgroup Generic_FSM_Exported_Functions Generic FSM Exported Functions
 * @{
 */

/**
* @brief  This function sets a new event to a FSM
* @param  fsm: the state machine to set the new event
* @param  event: the new event to be set
* @retval None
*/
inline void fsm_set_event(fsm_t *fsm, uint8_t event)
{
	if (fsm->event != FSM_EV_NO_EVENT) {
		//fsm_dbg("[FSM][%lX]Event: %d, %d\r\n", (uint32_t)fsm, fsm->event, event);
	}
	fsm->event = event;
}

/**
* @brief  This function returns the current state of a FSM
* @param  fsm: the state machine to get the state
* @retval state: Current FSM's state
*/
inline uint8_t fsm_get_state(fsm_t *fsm)
{
	return fsm->state;
}

/**
* @brief  This function returns the current event of a FSM
* @param  fsm: the state machine to get the state
* @retval state: Current FSM's state
*/
inline uint8_t fsm_get_event(fsm_t *fsm)
{
	return fsm->event;
}

/**
* @brief  This function returns if the FSM is running or not
* @param  fsm: the state machine to get the state
* @retval is_running: 1: the FSM is running, 0: otherwise.
*/
inline uint8_t fsm_is_running(fsm_t *fsm)
{
	return fsm->is_running;
}

/**
* @brief  This function enables a fsm.
* @param  fsm: the state machine to enable.
* @retval None.
*/
inline void fsm_enable(fsm_t *fsm)
{
	fsm->is_running = 1;
}

/**
* @brief  This function enables a fsm.
* @param  fsm: the state machine to enable.
* @retval None.
*/
inline void fsm_disable(fsm_t *fsm)
{
	fsm->is_running = 0;
}

void fsm_init(fsm_t *, fsm_init_t *);
void fsm_run(fsm_t *);

/** @} */

/** @} */

#endif /* __FSM_ */

/********************************* END OF FILE ********************************/
