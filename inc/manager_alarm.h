/**
  ******************************************************************************
  * @file   manager_alarm.h
  * @author samuel.cifuentes@titoma.com
  * @brief  Functions and types for the alarm management.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ALARM_H_
#define __ALARM_H_

/* Includes -------------------------------------------------------------------*/
#include <stdint.h>

#include "main.h"
#include "fsm.h"
#include "led_driver.h"

/* Exported constants --------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @defgroup alarm_Manager alarm
 * @{
 */

typedef enum alarm_states_ {
	ALARM_ST_ERROR = 0,
	ALARM_ST_IDLE,
	ALARM_ST_HIGH,
	ALARM_ST_LOW,
	ALARM_ST_INACTIVE,
	ALARM_ST_TOTAL
	
}alarm_states_t;

typedef enum alarm_events_ {
	ALARM_EV_ERROR = 0,
	ALARM_EV_NO_EVENT, // 1
	ALARM_EV_RUN_INIT,
	ALARM_EV_HIGH_INIT,
	ALARM_EV_HIGH_STOP,
	ALARM_EV_ACTIVE_STOP,
	ALARM_EV_RUN_STOP,
	ALARM_EV_TOTAL
	
}alarm_events_t;

typedef struct alarm_config_ {
	uint8_t     low_frequency;      //Hz
	uint8_t     low_duty_cycle;     //%

	uint16_t    on_time;            //ms
	uint16_t    off_time;           //ms

	uint32_t    pattern_time;       //ms

} alarm_config_t;

typedef struct alarm_time_ {
	uint16_t              high;
	uint16_t              low;
	uint32_t              high_init;
	uint32_t              high_stop;
	
	uint16_t              active;
	uint16_t              inactive;
	uint32_t              active_stop;
	
	uint32_t              pattern_stop;
	
} alarm_time_t;

typedef struct alarm_mcu_interface_ {
	led_t led;

} alarm_mcu_interface_t;

typedef struct alarm_manager_ {
	alarm_mcu_interface_t mcu_intf;
	fsm_t                    fsm;
	
	alarm_config_t  config;
	alarm_time_t    time;
	
} alarm_manager_t;

/**
 * @defgroup alarm_Manager_Exported          alarm Manager Exported Functions
 * @{
 */

inline void alarm_enable(alarm_manager_t *alarm) {
	led_driver_set(alarm->mcu_intf.led);
}

inline void alarm_disable(alarm_manager_t *alarm) {
	led_driver_clr(alarm->mcu_intf.led);
}

void alarm_set_pattern(alarm_manager_t *, alarm_config_t *);
void alarm_stop_pattern(alarm_manager_t *);
void alarm_manager_test(alarm_manager_t *, alarm_manager_t *);

/**
 * @}
 */

/**
 * @defgroup alarm_Manager_interface     alarm Manager Interface Functions
 * @{
 */

void alarm_manager_init(alarm_manager_t *, led_t);

inline void turn_manager_run(alarm_manager_t *alarm)
{
	if(alarm->fsm.is_running != 0) {
		fsm_run(&alarm->fsm);
	}
}

inline uint8_t alarm_is_busy(alarm_manager_t *alarm)
{
	return alarm->fsm.is_running;
}

/**
 * @}
 */

/**
 * @}
 */


#endif /* __ALARM_H_ */

/********************************* END OF FILE *********************************/
